- https://api.mullvad.net/public/documentation/

- https://api.mullvad.net/public/relays/wireguard/v2/

```json
{
  "locations": {
    "se-got": {
      "city": "Gothenburg",
      "country": "Sweden",
      "latitude": 57.70887,
      "longitude": 11.97456
    },
    "se-sto": {
      "city": "Stockholm",
      "country": "Sweden",
      "latitude": 57.70887,
      "longitude": 11.97456
    }
  },
  "wireguard": {
    "port_ranges": [
      [
        53,
        53
      ],
      [
        123,
        123
      ],
      [
        4000,
        33433
      ],
      [
        33565,
        51820
      ],
      [
        52000,
        60000
      ]
    ],
    "ipv4_gateway": "10.64.0.1",
    "ipv6_gateway": "fc00:bbbb:bbbb:bb01::1",
    "relays": [
      {
        "hostname": "se3-wireguard",
        "active": true,
        "owned": true,
        "location": "se-got",
        "provider": "31173",
        "ipv4_addr_in": "185.213.154.66",
        "ipv6_addr_in": "2a03:1b20:5:f011:31::a03f",
        "weight": 100,
        "include_in_country": true,
        "public_key": "5JMPeO7gXIbR5CnUa/NPNK4L5GqUnreF0/Bozai4pl4="
      }
    ]
  }
}
```

- https://api.mullvad.net/public/relays/wireguard/v1/

```json
{
  "countries": [
    {
      "name": "Sweden",
      "code": "se",
      "cities": [
        {
          "name": "Gothenburg",
          "code": "got",
          "latitude": 57.70887,
          "longitude": 11.97456,
          "relays": [
            {
              "hostname": "se3-wireguard",
              "ipv4_addr_in": "185.213.154.66",
              "ipv6_addr_in": "2a03:1b20:5:f011:31::a03f",
              "public_key": "5JMPeO7gXIbR5CnUa/NPNK4L5GqUnreF0/Bozai4pl4=",
              "multihop_port": 3020
            }
          ]
        }
      ]
    }
  ]
}
```
