package main

import (
	"bufio"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/valyala/fastjson"
)

const MLVDAPIURLV1 string = "https://api.mullvad.net/public/relays/wireguard/v1/"
const MLVDAPIPATHV1 string = "v1-relays.json"
const MLVDAPIURLV2 string = "https://api.mullvad.net/public/relays/wireguard/v2/"
const MLVDAPIPATHV2 string = "v2-relays.json"
const MLVDAPIFETCHINTERVALHOURS = 2
const MLVDAPIPORT = uint16(51821)
const MLVDSOCKS5PATH string = "socks5-relays.txt"
const MLVDINTERFACEPREFIX string = "mlvd"

var DefClient = &http.Client{
	Timeout: time.Second * 4,
}

type Slot struct {
	Id          string `json:"id"`
	WGInterface string `json:"wg_interface"`
}

type Slots map[string]*Slot

func (s *Slots) Contains(id string) bool {
	_, ok := (*s)[id]
	return ok
}

func (s *Slots) Get(id string) *Slot {
	sl := (*s)[id]
	return sl
}

func ReadSlots() (Slots, error) {
	const sys_net string = "/sys/class/net"
	fentries, err := os.ReadDir(sys_net)
	if err != nil {
		return nil, err
	}
	slots := make(Slots, len(fentries))
	i := uint64(1)
	for _, fentry := range fentries {
		if !strings.HasPrefix(fentry.Name(), MLVDINTERFACEPREFIX) {
			continue
		}
		fuevt, err := os.Open(path.Join(sys_net, fentry.Name(), "uevent"))
		if err != nil {
			continue
		}
		var isWG bool
		var iface string
		suevt := bufio.NewScanner(fuevt)
		for (!isWG || iface == "") && suevt.Scan() {
			stuevt := strings.SplitN(suevt.Text(), "=", 2)
			if len(stuevt) < 2 {
				continue
			}
			itype := strings.TrimSpace(stuevt[0])
			if strings.EqualFold(itype, "DEVTYPE") {
				isWG = strings.EqualFold(strings.TrimSpace(stuevt[1]), "wireguard")
			} else if strings.EqualFold(itype, "INTERFACE") {
				iface = strings.TrimSpace(stuevt[1])
			}
		}
		fuevt.Close()
		if isWG && iface != "" {
			sI := strconv.FormatUint(i, 10)
			slots[sI] = &Slot{
				Id:          sI,
				WGInterface: iface,
			}
			i++
		}
	}
	return slots, nil
}

type MlvdWGRelaysV2Location struct {
	v         *fastjson.Value
	City      string  `json:"city"`
	Country   string  `json:"country"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func (m *MlvdWGRelaysV2Location) fill(v *fastjson.Value) {
	m.v = v
	m.City = string(v.GetStringBytes("city"))
	m.Country = string(v.GetStringBytes("country"))
	m.Latitude = v.GetFloat64("latitude")
	m.Longitude = v.GetFloat64("longitude")
}

type MlvdWGRelaysV2Relay struct {
	v                *fastjson.Value
	Hostname         string `json:"hostname"`
	Active           bool   `json:"active"`
	Owned            bool   `json:"owned"`
	Location         string `json:"location"`
	Provider         string `json:"provider"`
	Ipv4AddrIn       string `json:"ipv4_addr_in"`
	Ipv6AddrIn       string `json:"ipv6_addr_in"`
	Weight           int    `json:"weight"`
	IncludeInCountry bool   `json:"include_in_country"`
	PublicKey        string `json:"public_key"`
}

func (m *MlvdWGRelaysV2Relay) fill(v *fastjson.Value) {
	m.v = v
	m.Hostname = string(v.GetStringBytes("hostname"))
	m.Active = v.GetBool("active")
	m.Owned = v.GetBool("owned")
	m.Location = string(v.GetStringBytes("location"))
	m.Provider = string(v.GetStringBytes("provider"))
	m.Ipv4AddrIn = string(v.GetStringBytes("ipv4_addr_in"))
	m.Ipv6AddrIn = string(v.GetStringBytes("ipv6_addr_in"))
	m.Weight = v.GetInt("weight")
	m.IncludeInCountry = v.GetBool("include_in_country")
	m.PublicKey = string(v.GetStringBytes("public_key"))
}

func (m *MlvdWGRelaysV2Relay) DomainName() string {
	return m.Hostname + ".relays.mullvad.net"
}

func (m *MlvdWGRelaysV2Relay) Socks5ProxyAddress() string {
	i := strings.LastIndex(m.Hostname, "-")
	return "socks5://" + m.Hostname[:i] + "-socks5" + m.Hostname[i:] + ".relays.mullvad.net:1080"
}

func (m *MlvdWGRelaysV2Relay) Ownership() string {
	if m.Owned {
		return "owned"
	} else {
		return "rented"
	}
}

type MlvdWGRelaysV2Wireguard struct {
	v           *fastjson.Value
	PortRanges  [][]uint16             `json:"port_ranges"`
	Ipv4Gateway string                 `json:"ipv4_gateway"`
	Ipv6Gateway string                 `json:"ipv6_gateway"`
	Relays      []*MlvdWGRelaysV2Relay `json:"relays"`
}

func (m *MlvdWGRelaysV2Wireguard) fill(v *fastjson.Value) {
	m.v = v
	m.Ipv4Gateway = string(v.GetStringBytes("ipv4_gateway"))
	m.Ipv6Gateway = string(v.GetStringBytes("ipv6_gateway"))
}

func (m *MlvdWGRelaysV2Wireguard) Socks5Proxies() []string {
	fjrelays := m.v.GetArray("relays")
	if len(fjrelays) == 0 {
		return nil
	}
	s5prox := make([]string, 0, len(fjrelays))
	var r MlvdWGRelaysV2Relay
	for _, fjrel := range fjrelays {
		r.fill(fjrel)
		if r.Active {
			s5prox = append(s5prox, r.Socks5ProxyAddress())
		}
	}
	return s5prox
}

func (m *MlvdWGRelaysV2Wireguard) FindRelay(hostname string) *MlvdWGRelaysV2Relay {
	fjrelays := m.v.GetArray("relays")
	if len(fjrelays) == 0 {
		return nil
	}
	hostname = strings.ToLower(strings.TrimSpace(hostname))
	for _, fjrel := range fjrelays {
		if strings.HasPrefix(strings.ToLower(string(fjrel.GetStringBytes("hostname"))), hostname) {
			r := new(MlvdWGRelaysV2Relay)
			r.fill(fjrel)
			m.Relays = []*MlvdWGRelaysV2Relay{r}
			return r
		}
	}
	return nil
}

func (m *MlvdWGRelaysV2Wireguard) FindRelayMulti(multi string) *MlvdWGRelaysV2Relay {
	fjrelays := m.v.GetArray("relays")
	if len(fjrelays) == 0 {
		return nil
	}
	multi = strings.TrimSpace(multi)
	multi = strings.Trim(multi, "[]")
	multi = strings.TrimSuffix(multi, ".relays.mullvad.net")
	for _, fjrel := range fjrelays {
		if strings.HasPrefix(strings.ToLower(string(fjrel.GetStringBytes("hostname"))), strings.ToLower(multi)) ||
			string(fjrel.GetStringBytes("public_key")) == multi ||
			string(fjrel.GetStringBytes("ipv4_addr_in")) == multi ||
			string(fjrel.GetStringBytes("ipv6_addr_in")) == multi {
			r := new(MlvdWGRelaysV2Relay)
			r.fill(fjrel)
			m.Relays = []*MlvdWGRelaysV2Relay{r}
			return r
		}
	}
	return nil
}

func (m *MlvdWGRelaysV2Wireguard) PickRelay(location string) *MlvdWGRelaysV2Relay {
	fjrelays := m.v.GetArray("relays")
	if len(fjrelays) == 0 {
		return nil
	}
	fjactiveRels := make([]*fastjson.Value, 0, len(fjrelays))
	for _, fjrel := range fjrelays {
		if fjrel.GetBool("active") && string(fjrel.GetStringBytes("location")) == location {
			fjactiveRels = append(fjactiveRels, fjrel)
		}
	}
	if len(fjactiveRels) == 0 {
		return nil
	}
	var fjrel *fastjson.Value
	i, err := rand.Int(rand.Reader, big.NewInt(int64(len(fjactiveRels))))
	if err != nil {
		fjrel = fjactiveRels[0]
	} else {
		fjrel = fjactiveRels[i.Uint64()]
	}
	r := new(MlvdWGRelaysV2Relay)
	r.fill(fjrel)
	m.Relays = []*MlvdWGRelaysV2Relay{r}
	return r
}

type MlvdWGRelaysV2 struct {
	p         *fastjson.ParserPool
	v         *fastjson.Value
	Locations map[string]*MlvdWGRelaysV2Location `json:"locations"`
	Wireguard *MlvdWGRelaysV2Wireguard           `json:"wireguard"`
}

func (m *MlvdWGRelaysV2) MarshalJSON() ([]byte, error) {
	return json.Marshal(m)
}

func (m *MlvdWGRelaysV2) MarshalJSONWriter(w io.Writer) error {
	return json.NewEncoder(w).Encode(m)
}

func (m *MlvdWGRelaysV2) UnmarshalJSON(b []byte) error {
	if m.p == nil {
		m.p = new(fastjson.ParserPool)
	}
	p := m.p.Get()
	var err error
	m.v, err = p.ParseBytes(b)
	m.p.Put(p)
	if err != nil {
		return err
	}
	w := m.v.Get("wireguard")
	m.Wireguard = new(MlvdWGRelaysV2Wireguard)
	m.Wireguard.fill(w)
	return nil
}

func (m *MlvdWGRelaysV2) UnmarshalJSONReader(r io.Reader) error {
	b, err := io.ReadAll(r)
	if err != nil {
		return err
	}
	return m.UnmarshalJSON(b)
}

func (m *MlvdWGRelaysV2) FindLocationObject(location string) *MlvdWGRelaysV2Location {
	fjlocation := m.v.Get("locations", location)
	if fjlocation == nil {
		return nil
	}
	l := new(MlvdWGRelaysV2Location)
	l.fill(fjlocation)
	m.Locations = map[string]*MlvdWGRelaysV2Location{
		location: l,
	}
	return l
}

func (m *MlvdWGRelaysV2) FindLocation(country string, city string) string {
	fjlocations := m.v.GetObject("locations")
	if fjlocations == nil || fjlocations.Len() == 0 {
		return ""
	}
	country = strings.ToLower(strings.TrimSpace(country))
	city = strings.ToLower(strings.TrimSpace(city))
	var locKey string
	fjlocations.Visit(func(key []byte, v *fastjson.Value) {
		if locKey != "" {
			return
		}
		if strings.HasPrefix(strings.ToLower(string(v.GetStringBytes("country"))), country) &&
			strings.HasPrefix(strings.ToLower(string(v.GetStringBytes("city"))), city) {
			locKey = string(key)
			l := new(MlvdWGRelaysV2Location)
			l.fill(v)
			m.Locations = map[string]*MlvdWGRelaysV2Location{
				locKey: l,
			}
		}
	})
	return locKey
}

type MlvdWGRelaysV1Relay struct {
	v            *fastjson.Value
	Hostname     string `json:"hostname"`
	Ipv4AddrIn   string `json:"ipv4_addr_in"`
	Ipv6AddrIn   string `json:"ipv6_addr_in"`
	PublicKey    string `json:"public_key"`
	MultihopPort uint16 `json:"multihop_port"`
}

func (m *MlvdWGRelaysV1Relay) DomainName() string {
	return m.Hostname + ".relays.mullvad.net"
}

func (m *MlvdWGRelaysV1Relay) fill(v *fastjson.Value) {
	m.v = v
	m.Hostname = string(v.GetStringBytes("hostname"))
	m.Ipv4AddrIn = string(v.GetStringBytes("ipv4_addr_in"))
	m.Ipv6AddrIn = string(v.GetStringBytes("ipv6_addr_in"))
	m.PublicKey = string(v.GetStringBytes("public_key"))
	m.MultihopPort = uint16(v.GetUint("multihop_port"))
}

type MlvdWGRelaysV1City struct {
	v         *fastjson.Value
	Name      string                 `json:"name"`
	Code      string                 `json:"code"`
	Latitude  float64                `json:"latitude"`
	Longitude float64                `json:"longitude"`
	Relays    []*MlvdWGRelaysV1Relay `json:"relays"`
}

func (m *MlvdWGRelaysV1City) fill(v *fastjson.Value) {
	m.v = v
	m.Name = string(v.GetStringBytes("name"))
	m.Code = string(v.GetStringBytes("code"))
	m.Latitude = v.GetFloat64("latitude")
	m.Longitude = v.GetFloat64("longitude")
}

func (m *MlvdWGRelaysV1City) FindRelay(hostname string) *MlvdWGRelaysV1Relay {
	fjrelays := m.v.GetArray("relays")
	if len(fjrelays) == 0 {
		return nil
	}
	hostname = strings.ToLower(strings.TrimSpace(hostname))
	for _, fjrel := range fjrelays {
		if strings.HasPrefix(strings.ToLower(string(fjrel.GetStringBytes("hostname"))), hostname) {
			r := new(MlvdWGRelaysV1Relay)
			r.fill(fjrel)
			m.Relays = []*MlvdWGRelaysV1Relay{r}
			return r
		}
	}
	return nil
}

type MlvdWGRelaysV1Country struct {
	v      *fastjson.Value
	Name   string                `json:"name"`
	Code   string                `json:"code"`
	Cities []*MlvdWGRelaysV1City `json:"cities"`
}

func (m *MlvdWGRelaysV1Country) fill(v *fastjson.Value) {
	m.v = v
	m.Name = string(v.GetStringBytes("name"))
	m.Code = string(v.GetStringBytes("code"))
}

func (m *MlvdWGRelaysV1Country) FindCity(city string) *MlvdWGRelaysV1City {
	fjcities := m.v.GetArray("cities")
	if len(fjcities) == 0 {
		return nil
	}
	city = strings.ToLower(strings.TrimSpace(city))
	if city == "" {
		return nil
	}
	for _, fjci := range fjcities {
		if (len(city) <= 3 && strings.HasPrefix(strings.ToLower(string(fjci.GetStringBytes("code"))), city)) ||
			strings.HasPrefix(strings.ToLower(string(fjci.GetStringBytes("name"))), city) {
			c := new(MlvdWGRelaysV1City)
			c.fill(fjci)
			m.Cities = []*MlvdWGRelaysV1City{c}
			return c
		}
	}
	return nil
}

func (m *MlvdWGRelaysV1Country) PickCity() *MlvdWGRelaysV1City {
	fjcities := m.v.GetArray("cities")
	if len(fjcities) == 0 {
		return nil
	}
	var fjcity *fastjson.Value
	i, err := rand.Int(rand.Reader, big.NewInt(int64(len(fjcities))))
	if err != nil {
		fjcity = fjcities[0]
	} else {
		fjcity = fjcities[i.Uint64()]
	}
	c := new(MlvdWGRelaysV1City)
	c.fill(fjcity)
	m.Cities = []*MlvdWGRelaysV1City{c}
	return c
}

type MlvdWGRelaysV1 struct {
	p         *fastjson.ParserPool
	v         *fastjson.Value
	Countries []*MlvdWGRelaysV1Country `json:"countries"`
}

func (m *MlvdWGRelaysV1) FindCountry(country string) *MlvdWGRelaysV1Country {
	fjcountries := m.v.GetArray("countries")
	if len(fjcountries) == 0 {
		return nil
	}
	country = strings.ToLower(strings.TrimSpace(country))
	if country == "" {
		return nil
	}
	for _, fjco := range fjcountries {
		fjco.GetStringBytes("code")
		if (len(country) <= 2 && strings.HasPrefix(strings.ToLower(string(fjco.GetStringBytes("code"))), country)) ||
			strings.HasPrefix(strings.ToLower(string(fjco.GetStringBytes("name"))), country) {
			c := new(MlvdWGRelaysV1Country)
			c.fill(fjco)
			m.Countries = []*MlvdWGRelaysV1Country{c}
			return c
		}
	}
	return nil
}

func (m *MlvdWGRelaysV1) PickCountry(countrybl []string) *MlvdWGRelaysV1Country {
	fjcountries := m.v.GetArray("countries")
	if len(fjcountries) == 0 {
		return nil
	}
	if len(countrybl) > 0 {
		tmpfjcountries := fjcountries
		fjcountries = make([]*fastjson.Value, 0, len(tmpfjcountries))
		var c MlvdWGRelaysV1Country
		for _, v := range tmpfjcountries {
			c.fill(v)
			bl := false
			for _, blc := range countrybl {
				if (len(blc) <= 2 && strings.HasPrefix(strings.ToLower(c.Code), blc)) ||
					strings.HasPrefix(strings.ToLower(c.Name), blc) {
					bl = true
					break
				}
			}
			if !bl {
				fjcountries = append(fjcountries, v)
			}
		}
	}
	var fjcountry *fastjson.Value
	i, err := rand.Int(rand.Reader, big.NewInt(int64(len(fjcountries))))
	if err != nil {
		fjcountry = fjcountries[0]
	} else {
		fjcountry = fjcountries[i.Uint64()]
	}
	c := new(MlvdWGRelaysV1Country)
	c.fill(fjcountry)
	m.Countries = []*MlvdWGRelaysV1Country{c}
	return c
}

func (m *MlvdWGRelaysV1) MarshalJSON() ([]byte, error) {
	return json.Marshal(m)
}

func (m *MlvdWGRelaysV1) MarshalJSONWriter(w io.Writer) error {
	return json.NewEncoder(w).Encode(m)
}

func (m *MlvdWGRelaysV1) UnmarshalJSON(b []byte) error {
	if m.p == nil {
		m.p = new(fastjson.ParserPool)
	}
	p := m.p.Get()
	var err error
	m.v, err = p.ParseBytes(b)
	m.p.Put(p)
	return err
}

func (m *MlvdWGRelaysV1) UnmarshalJSONReader(r io.Reader) error {
	b, err := io.ReadAll(r)
	if err != nil {
		return err
	}
	return m.UnmarshalJSON(b)
}

type EndpointType int

const (
	Domain EndpointType = iota
	Ipv4
	Ipv6
	Unknown
)

func (et EndpointType) String() string {
	switch et {
	case Domain:
		return "domain"
	case Ipv4:
		return "ipv4"
	case Ipv6:
		return "ipv6"
	default:
		return ""
	}
}

func ParseEndpointType(s string) (EndpointType, error) {
	s = strings.ToLower(strings.TrimSpace(s))
	for i := Domain; i < Unknown; i++ {
		if s == i.String() {
			return i, nil
		}
	}
	return Unknown, fmt.Errorf("unknown endpoint type '%s'", s)
}

type apiResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message,omitempty"`
	Data    any    `json:"data,omitempty"`
}

func (ar apiResponse) Write(rw http.ResponseWriter, statusCode int) error {
	rw.Header().Set("Content-Type", "application/json; charset=utf-8")
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.WriteHeader(statusCode)
	b, err := json.Marshal(&ar)
	if err != nil {
		return err
	}
	_, err = rw.Write(b)
	return err
}

func applyWGSettings(slot string, relV1Exit *MlvdWGRelaysV1Relay, relV1Hop *MlvdWGRelaysV1Relay, et EndpointType) error {
	AvailableSlots, err := ReadSlots()
	if err != nil {
		return err
	}
	sl := AvailableSlots.Get(slot)
	if sl == nil {
		return fmt.Errorf("unknown slot '%s'", slot)
	}
	cmd := exec.Command("wg", "show", sl.WGInterface, "peers")
	out, err := cmd.CombinedOutput()
	sout := strings.TrimSpace(string(out))
	if len(sout) > 0 {
		log.Println(sout)
	}
	if err != nil {
		return fmt.Errorf("error running wg: %w: %s", err, sout)
	}
	peers := strings.Split(sout, "\n")
	for _, p := range peers {
		p = strings.TrimSpace(p)
		if p == "" {
			continue
		}
		cmd = exec.Command("wg", "set", sl.WGInterface, "peer", p, "remove")
		out, err = cmd.CombinedOutput()
		sout = strings.TrimSpace(string(out))
		if len(sout) > 0 {
			log.Println(sout)
		}
		if err != nil {
			return fmt.Errorf("error running wg: %w: %s", err, sout)
		}
	}
	args := []string{
		"set", sl.WGInterface,
		"peer", relV1Exit.PublicKey,
		"endpoint", "",
		"allowed-ips", "0.0.0.0/0,::/0",
	}
	if relV1Hop != nil {
		switch et {
		case Domain, Unknown:
			args[5] = fmt.Sprintf("%s:%d", relV1Hop.DomainName(), relV1Exit.MultihopPort)
		case Ipv4:
			args[5] = fmt.Sprintf("%s:%d", relV1Hop.Ipv4AddrIn, relV1Exit.MultihopPort)
		case Ipv6:
			args[5] = fmt.Sprintf("[%s]:%d", relV1Hop.Ipv6AddrIn, relV1Exit.MultihopPort)
		}
	} else {
		switch et {
		case Domain, Unknown:
			args[5] = fmt.Sprintf("%s:%d", relV1Exit.DomainName(), 51820)
		case Ipv4:
			args[5] = fmt.Sprintf("%s:%d", relV1Exit.Ipv4AddrIn, 51820)
		case Ipv6:
			args[5] = fmt.Sprintf("[%s]:%d", relV1Exit.Ipv6AddrIn, 51820)
		}
	}
	atmp := make([]any, len(args)+1)
	atmp[0] = "wg"
	for i, a := range args {
		atmp[i+1] = a
	}
	log.Println(atmp...)
	cmd = exec.Command("wg", args...)
	out, err = cmd.CombinedOutput()
	if len(out) > 0 {
		log.Println(strings.TrimSpace(string(out)))
	}
	if err != nil {
		return fmt.Errorf("error running wg: %w: %s", err, strings.TrimSpace(string(out)))
	}
	return nil
}

func fetchAPIV1() (*MlvdWGRelaysV1, error) {
	finfo, err := os.Stat(MLVDAPIPATHV1)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return nil, fmt.Errorf("error while stating file '%s': %w", MLVDAPIPATHV1, err)
	}
	var b []byte
	if finfo == nil || time.Since(finfo.ModTime()).Hours() > MLVDAPIFETCHINTERVALHOURS {
		var resp *http.Response
		resp, err = DefClient.Get(MLVDAPIURLV1)
		if err != nil {
			log.Println(fmt.Errorf("error while requesting the mullvad api v1: %w", err))
		} else {
			b, err = io.ReadAll(resp.Body)
			resp.Body.Close()
			if err != nil {
				b = nil
				log.Println(fmt.Errorf("error while reading the mullvad api v1: %w", err))
			} else if len(b) < 256 {
				b = nil
				log.Println(fmt.Errorf("error while reading the mullvad api v1: throttled"))
			} else {
				err = os.WriteFile(MLVDAPIPATHV1, b, 0666)
				if err != nil {
					log.Println(fmt.Errorf("error writing file '%s': %w", MLVDAPIPATHV1, err))
				}
			}
		}
	}
	if len(b) == 0 {
		b, err = os.ReadFile(MLVDAPIPATHV1)
		if err != nil {
			return nil, fmt.Errorf("error while reading file '%s': %w", MLVDAPIPATHV1, err)
		}
	}
	relsV1 := new(MlvdWGRelaysV1)
	err = relsV1.UnmarshalJSON(b)
	if err != nil {
		return nil, fmt.Errorf("error while parsing the mullvad api v1 json: %w", err)
	}
	return relsV1, nil
}

func fetchAPIV2() (*MlvdWGRelaysV2, error) {
	finfo, err := os.Stat(MLVDAPIPATHV2)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return nil, fmt.Errorf("error while stating file '%s': %w", MLVDAPIPATHV2, err)
	}
	update := false
	var b []byte
	if finfo == nil || time.Since(finfo.ModTime()).Hours() > MLVDAPIFETCHINTERVALHOURS {
		var resp *http.Response
		resp, err = DefClient.Get(MLVDAPIURLV2)
		if err != nil {
			log.Println(fmt.Errorf("error while requesting the mullvad api v2: %w", err))
		} else {
			b, err = io.ReadAll(resp.Body)
			resp.Body.Close()
			if err != nil {
				b = nil
				log.Println(fmt.Errorf("error while reading the mullvad api v2: %w", err))
			} else if len(b) < 256 {
				b = nil
				log.Println(fmt.Errorf("error while reading the mullvad api v2: throttled"))
			} else {
				err = os.WriteFile(MLVDAPIPATHV2, b, 0666)
				if err != nil {
					log.Println(fmt.Errorf("error writing file '%s': %w", MLVDAPIPATHV2, err))
				}
				update = true
			}
		}
	}
	if len(b) == 0 {
		b, err = os.ReadFile(MLVDAPIPATHV2)
		if err != nil {
			return nil, fmt.Errorf("error while reading file '%s': %w", MLVDAPIPATHV2, err)
		}
	}
	relsV2 := new(MlvdWGRelaysV2)
	err = relsV2.UnmarshalJSON(b)
	if err != nil {
		return nil, fmt.Errorf("error while parsing the mullvad api v2 json: %w", err)
	}
	_, err = os.Stat(MLVDSOCKS5PATH)
	if update || err != nil {
		err = writeSocks5Proxies(relsV2)
		if err != nil {
			log.Println(err)
		}
	}
	return relsV2, nil
}

func writeSocks5Proxies(relsV2 *MlvdWGRelaysV2) error {
	s5proxies := relsV2.Wireguard.Socks5Proxies()
	f, err := os.Create(MLVDSOCKS5PATH)
	if err != nil {
		return fmt.Errorf("error writing file '%s': %w", MLVDSOCKS5PATH, err)
	}
	defer f.Close()
	for _, s5prox := range s5proxies {
		_, err = fmt.Fprintln(f, s5prox)
		if err != nil {
			return fmt.Errorf("error writing file '%s': %w", MLVDSOCKS5PATH, err)
		}
	}
	return nil
}

func pickRelays(hostname, country, city string, relsV1 *MlvdWGRelaysV1, relsV2 *MlvdWGRelaysV2, countrybl []string) (
	relV1 *MlvdWGRelaysV1Relay, relV2 *MlvdWGRelaysV2Relay, err error) {
	hostname = strings.TrimSpace(hostname)
	if hostname != "" {
		relV2 = relsV2.Wireguard.FindRelay(hostname)
		if relV2 == nil {
			err = fmt.Errorf("could not find hostname '%s'", hostname)
			return
		}
		loc := relsV2.FindLocationObject(relV2.Location)
		if loc == nil {
			err = fmt.Errorf("could not find location object '%s'", relV2.Location)
			return
		}
		country = loc.Country
		city = loc.City
	}
	var co *MlvdWGRelaysV1Country
	country = strings.TrimSpace(country)
	if country != "" {
		co = relsV1.FindCountry(country)
		if co == nil {
			err = fmt.Errorf("could not find country '%s'", country)
			return
		}
	} else {
		co = relsV1.PickCountry(countrybl)
		if co == nil {
			err = fmt.Errorf("could not pick country")
			return
		}
	}
	var ci *MlvdWGRelaysV1City
	city = strings.TrimSpace(city)
	if city != "" {
		ci = co.FindCity(city)
		if ci == nil {
			err = fmt.Errorf("could not find city '%s'", city)
			return
		}
	} else {
		ci = co.PickCity()
		if ci == nil {
			err = fmt.Errorf("could not pick city")
			return
		}
	}
	if relV2 == nil {
		location := relsV2.FindLocation(co.Name, ci.Name)
		if location == "" {
			err = fmt.Errorf("could not find location")
			return
		}
		relV2 = relsV2.Wireguard.PickRelay(location)
		if relV2 == nil {
			err = fmt.Errorf("could not pick v2 relay")
			return
		}
	}
	relV1 = ci.FindRelay(relV2.Hostname)
	if relV1 == nil {
		err = fmt.Errorf("could not find v1 relay")
		return
	}
	return
}

type APIV1WGRelayInfo struct {
	Hostname   string `json:"hostname"`
	DomainName string `json:"domain_name"`
	Ipv4AddrIn string `json:"ipv4_addr_in"`
	Ipv6AddrIn string `json:"ipv6_addr_in"`
	Country    string `json:"country"`
	City       string `json:"city"`
	Provider   string `json:"provider"`
	Ownership  string `json:"ownership"`
}

type APIV1WGSlotInfo struct {
	Hop      *APIV1WGRelayInfo `json:"hop,omitempty"`
	Exit     *APIV1WGRelayInfo `json:"exit"`
	Multihop bool              `json:"multihop"`
}

type APIV1WGSlotsInfo struct {
	Slots map[string]*APIV1WGSlotInfo `json:"slots"`
}

func (a *APIV1WGSlotsInfo) Fill(relsV2 *MlvdWGRelaysV2) error {
	AvailableSlots, err := ReadSlots()
	if err != nil {
		return err
	}
	a.Slots = make(map[string]*APIV1WGSlotInfo, len(AvailableSlots))
	for k, v := range AvailableSlots {
		log.Println("wg", "show", v.WGInterface, "endpoints")
		cmd := exec.Command("wg", "show", v.WGInterface, "endpoints")
		out, err := cmd.CombinedOutput()
		sout := strings.TrimSpace(string(out))
		if len(sout) > 0 {
			log.Println(sout)
		}
		if err != nil {
			return fmt.Errorf("error running wg: %w: %s", err, sout)
		}
		sout, _, _ = strings.Cut(sout, "\n")
		if sout == "" {
			continue
		}
		soutf := strings.Fields(sout)
		if len(soutf) < 2 {
			return fmt.Errorf("unexpected output from wg")
		}
		publicKey := soutf[0]
		endpoint := soutf[1]
		eli := strings.LastIndex(endpoint, ":")
		if eli < 0 {
			eli = len(endpoint)
		}
		endpointAddr := endpoint[:eli]
		relV2Exit := relsV2.Wireguard.FindRelayMulti(publicKey)
		if relV2Exit == nil {
			return fmt.Errorf("could not find exit relay")
		}
		relV2Hop := relsV2.Wireguard.FindRelayMulti(endpointAddr)
		if relV2Hop == nil {
			return fmt.Errorf("could not find hop relay")
		}
		relV2ExitLoc := relsV2.FindLocationObject(relV2Exit.Location)
		if relV2ExitLoc == nil {
			return fmt.Errorf("could not find exit location")
		}
		relV2HopLoc := relsV2.FindLocationObject(relV2Hop.Location)
		if relV2HopLoc == nil {
			return fmt.Errorf("could not find hop location")
		}
		if relV2Exit.Hostname != relV2Hop.Hostname {
			a.Slots[k] = &APIV1WGSlotInfo{
				Hop: &APIV1WGRelayInfo{
					Hostname:   relV2Hop.Hostname,
					DomainName: relV2Hop.DomainName(),
					Ipv4AddrIn: relV2Hop.Ipv4AddrIn,
					Ipv6AddrIn: relV2Hop.Ipv6AddrIn,
					Country:    relV2HopLoc.Country,
					City:       relV2HopLoc.City,
					Provider:   relV2Hop.Provider,
					Ownership:  relV2Hop.Ownership(),
				},
				Exit: &APIV1WGRelayInfo{
					Hostname:   relV2Exit.Hostname,
					DomainName: relV2Exit.DomainName(),
					Ipv4AddrIn: relV2Exit.Ipv4AddrIn,
					Ipv6AddrIn: relV2Exit.Ipv6AddrIn,
					Country:    relV2ExitLoc.Country,
					City:       relV2ExitLoc.City,
					Provider:   relV2Exit.Provider,
					Ownership:  relV2Exit.Ownership(),
				},
				Multihop: true,
			}
		} else {
			a.Slots[k] = &APIV1WGSlotInfo{
				Exit: &APIV1WGRelayInfo{
					Hostname:   relV2Exit.Hostname,
					DomainName: relV2Exit.DomainName(),
					Ipv4AddrIn: relV2Exit.Ipv4AddrIn,
					Ipv6AddrIn: relV2Exit.Ipv6AddrIn,
					Country:    relV2ExitLoc.Country,
					City:       relV2ExitLoc.City,
					Provider:   relV2Exit.Provider,
					Ownership:  relV2Exit.Ownership(),
				},
				Multihop: false,
			}
		}
	}
	return nil
}

type apiv1InfoHandler struct {
}

func (a *apiv1InfoHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
	case http.MethodHead, http.MethodOptions:
		apiResponse{
			Success: true,
		}.Write(rw, http.StatusOK)
		return
	default:
		apiResponse{
			Success: false,
			Message: "method not allowed",
		}.Write(rw, http.StatusMethodNotAllowed)
		return
	}
	err := req.ParseForm()
	if err != nil {
		err = fmt.Errorf("error while parsing the query: %w", err)
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusBadRequest)
		return
	}
	var relsV2 *MlvdWGRelaysV2
	relsV1C := make(chan *MlvdWGRelaysV1)
	relsV2C := make(chan *MlvdWGRelaysV2)
	relsCE := make(chan error, 2)
	go func() {
		crelsV1, cerr := fetchAPIV1()
		if cerr != nil {
			relsCE <- cerr
		} else {
			relsV1C <- crelsV1
		}
	}()
	go func() {
		crelsV2, cerr := fetchAPIV2()
		if cerr != nil {
			relsCE <- cerr
		} else {
			relsV2C <- crelsV2
		}
	}()
	for i := 0; i < 2; i++ {
		select {
		case err = <-relsCE:
			log.Println(err)
			apiResponse{
				Success: false,
				Message: err.Error(),
			}.Write(rw, http.StatusInternalServerError)
			return
		case <-relsV1C:
		case relsV2 = <-relsV2C:
		}
	}

	slotsInfo := new(APIV1WGSlotsInfo)
	err = slotsInfo.Fill(relsV2)
	if err != nil {
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusInternalServerError)
		return
	}
	apiResponse{
		Success: true,
		Data:    slotsInfo,
	}.Write(rw, http.StatusOK)
}

type apiv1SwitchHandler struct {
}

// Query params:
//   - slot: which slot to work on (required)
//   - hostname: specific exit hostname to pick
//     ignores any country or city values if set
//     can be the short name (se3) or full name (se3-wireguard)
//   - country: code or name of the exit country to pick
//     if not specified, picks a random country
//   - city: code or name of the exit city to pick
//     if not specified, picks a random city
//   - multihop: enables multihop
//     if *_hop are not specified picks a random relay for first hop
//   - hostname_hop: specific first hop hostname to pick
//     ignores any country_hop or city_hop values if set
//     can be the short name (se3) or full name (se3-wireguard)
//     implicitly enables multihop
//   - country_hop: code or name of the first hop country to pick
//     if not specified, picks a random country
//     implicitly enables multihop
//   - city_hop: code or name of the first hop city to pick
//     if not specified, picks a random city
//     implicitly enables multihop
//   - endpoint_type: one of domain, ipv4, ipv6
//   - country_blacklist: comma separated list of blacklisted countries
//     e.g.: fr,de,us
func (a *apiv1SwitchHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet, http.MethodPost:
	case http.MethodHead, http.MethodOptions:
		apiResponse{
			Success: true,
		}.Write(rw, http.StatusOK)
		return
	default:
		apiResponse{
			Success: false,
			Message: "method not allowed",
		}.Write(rw, http.StatusMethodNotAllowed)
		return
	}
	err := req.ParseForm()
	if err != nil {
		err = fmt.Errorf("error while parsing the query: %w", err)
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusBadRequest)
		return
	}
	slot := strings.TrimSpace(req.Form.Get("slot"))
	if slot == "" {
		err = fmt.Errorf("missing required parameter 'slot'")
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusBadRequest)
		return
	}
	var AvailableSlots Slots
	AvailableSlots, err = ReadSlots()
	if err != nil {
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusInternalServerError)
		return
	}
	if !AvailableSlots.Contains(slot) {
		err = fmt.Errorf("unknown slot '%s'", slot)
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusBadRequest)
		return
	}
	et := Domain
	ets := strings.TrimSpace(req.Form.Get("endpoint_type"))
	if ets != "" {
		et, err = ParseEndpointType(ets)
		if err != nil {
			err = fmt.Errorf("'endpoint_type' parameter error: %w", err)
			log.Println(err)
			apiResponse{
				Success: false,
				Message: err.Error(),
			}.Write(rw, http.StatusBadRequest)
			return
		}
	}
	var countrybl []string
	csbls := strings.TrimSpace(req.Form.Get("country_blacklist"))
	if csbls != "" {
		csblsArr := strings.Split(csbls, ",")
		countrybl = make([]string, 0, len(csblsArr))
		for _, c := range csblsArr {
			c = strings.ToLower(strings.TrimSpace(c))
			if c != "" {
				countrybl = append(countrybl, c)
			}
		}
	}
	var relsV1 *MlvdWGRelaysV1
	var relsV2 *MlvdWGRelaysV2
	relsV1C := make(chan *MlvdWGRelaysV1)
	relsV2C := make(chan *MlvdWGRelaysV2)
	relsCE := make(chan error, 2)
	go func() {
		crelsV1, cerr := fetchAPIV1()
		if cerr != nil {
			relsCE <- cerr
		} else {
			relsV1C <- crelsV1
		}
	}()
	go func() {
		crelsV2, cerr := fetchAPIV2()
		if cerr != nil {
			relsCE <- cerr
		} else {
			relsV2C <- crelsV2
		}
	}()
	for i := 0; i < 2; i++ {
		select {
		case err = <-relsCE:
			log.Println(err)
			apiResponse{
				Success: false,
				Message: err.Error(),
			}.Write(rw, http.StatusInternalServerError)
			return
		case relsV1 = <-relsV1C:
		case relsV2 = <-relsV2C:
		}
	}

	multihop, _ := strconv.ParseBool(strings.TrimSpace(req.Form.Get("multihop")))
	multihop = multihop || len(strings.TrimSpace(req.Form.Get("hostname_hop"))) > 0 || len(strings.TrimSpace(req.Form.Get("country_hop"))) > 0 || len(strings.TrimSpace(req.Form.Get("city_hop"))) > 0

	var exitRelV1 *MlvdWGRelaysV1Relay
	var exitRelV2 *MlvdWGRelaysV2Relay
	exitRelV1, exitRelV2, err = pickRelays(req.Form.Get("hostname"), req.Form.Get("country"), req.Form.Get("city"), relsV1, relsV2, countrybl)
	if err != nil {
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusBadRequest)
		return
	}

	exitLoc := relsV2.FindLocationObject(exitRelV2.Location)

	var msg string
	var hopRelV1 *MlvdWGRelaysV1Relay
	var hopRelV2 *MlvdWGRelaysV2Relay
	if multihop {
		countrybl = append(countrybl, exitLoc.Country)
		hopRelV1, hopRelV2, err = pickRelays(req.Form.Get("hostname_hop"), req.Form.Get("country_hop"), req.Form.Get("city_hop"), relsV1, relsV2, countrybl)
		if err != nil {
			log.Println(err)
			apiResponse{
				Success: false,
				Message: err.Error(),
			}.Write(rw, http.StatusBadRequest)
			return
		}

		hopLoc := relsV2.FindLocationObject(hopRelV2.Location)
		msg = fmt.Sprintf("(slot%s) switching to multihop HOP '%s' (%s, %s, %s, %s) => EXIT '%s' (%s, %s, %s, %s)",
			slot,
			hopRelV2.DomainName(),
			hopLoc.Country,
			hopLoc.City,
			hopRelV2.Provider,
			hopRelV2.Ownership(),
			exitRelV2.DomainName(),
			exitLoc.Country,
			exitLoc.City,
			exitRelV2.Provider,
			exitRelV2.Ownership(),
		)
	} else {
		msg = fmt.Sprintf("(slot%s) switching to EXIT '%s' (%s, %s, %s, %s)",
			slot,
			exitRelV2.DomainName(),
			exitLoc.Country,
			exitLoc.City,
			exitRelV2.Provider,
			exitRelV2.Ownership(),
		)
	}
	log.Println(msg)
	err = applyWGSettings(slot, exitRelV1, hopRelV1, et)
	if err != nil {
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusInternalServerError)
		return
	}
	slotsInfo := new(APIV1WGSlotsInfo)
	err = slotsInfo.Fill(relsV2)
	if err != nil {
		log.Println(err)
		apiResponse{
			Success: false,
			Message: err.Error(),
		}.Write(rw, http.StatusInternalServerError)
		return
	}
	apiResponse{
		Success: true,
		Message: msg,
		Data:    slotsInfo,
	}.Write(rw, http.StatusOK)
}

func main() {
	http.Handle("/api/v1/wireguard/info", new(apiv1InfoHandler))
	http.Handle("/api/v1/wireguard/switch", new(apiv1SwitchHandler))
	http.ListenAndServe(":"+strconv.FormatUint(uint64(MLVDAPIPORT), 10), nil)
}
